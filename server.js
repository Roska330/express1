const fs = require('fs') //para el log
const path = require('path') //para recursos estáticos
const express = require('express')
const app = express()
const morgan = require('morgan') //middleware de log 

const hbs = require('hbs')
hbs.registerPartials(path.join(__dirname, 'views', 'partials'))
app.set('view engine', 'hbs'); // clave valor


//un par de middleware:
//morgan desarrollado por terceros
app.use(morgan('dev'))
//ese sencillo nuestro
app.use(function (req, res, next) {
    var now = new Date().toString()
    var log = `${now}: ${req.method} ${req.url} ${5 + 5}`
    console.log(log)
    fs.appendFile('server.log', `${log}\n`, (err) => {
      if (err) console.log(`No se ha podido usar el fichero de log:  ${err}`)
    })
    next()
})

//middleware para contenido estático
const publicRoute = path.join(__dirname, 'public')
app.use(express.static(publicRoute))

const staticRoute = path.join(__dirname, 'estatico')
app.use('/static', express.static(staticRoute))


app.get('/', (req, res) => {
    res.header("Content-Type", "text/html");
    res.send('Hola Mundo!')
})

// const contacto = require('./contacto.js')
const contacto = require('./contacto.json')

app.get('/contactar', (req, res) => {

    res.render('contactar.hbs', {
        pageTitle: 'Contactar',
        currentYear: new Date().getFullYear()
      })
    
    // res.json(contacto);
    // res.json({
    //     nombre: 'pepito',
    //     email: 'pepito@gmail.com'
    //   })
  
    // res.write('Contacto:')
    // res.write('Aquí la info de contacto')
    // res.send()
})

app.listen(3000, (err) => {
    console.log('Servidor escuchando en el puerto 3000');
    // if (err) {
    //     console.log(err);
    // } else {
    //     console.log('Servidor escuchando en el puerto 3000');
    // }
})

// app.listen(3000, function(err){
//     console.log('Servidor escuchando en el puerto 3000');
// })